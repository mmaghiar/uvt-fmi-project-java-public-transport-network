package ro.ptn.app;

import java.sql.Connection;
import java.sql.SQLException;

import ro.ptn.app.controller.AppController;
import ro.ptn.db.DbManager;
import ro.ptn.io.DbInput;
import ro.ptn.io.InputDevice;
import ro.ptn.io.OutputDevice;
/**
 * Application description
 * 
 * Project #2:
 * 
 * Additional requirements for the same context described in the first project (P1):
 * 1. also consider locations in the town where no transport line is available but the 
 *    person could walk (5km/h) from some of those places to others;
 * 2. find the shortest path between two locations, with minimal line switches
 * 3. import the transport network information for the Timisoara city available at TM 
 *    Transport into a relational database (MySQL, Derby, HSQL, SQLLite, etc)
 * 4. read the network configuration from the database and test the previously written 
 *    code on the new data: adapt the code if performance problems exist
 * 5. NOTE: also review the first submitted project version and consider the provided observations
 * 
 * Observations from the first assignment:
 * 
 *	- the search for optimal path is basically copy-pasted in the classes implementing Route: 
 *	  need a way to extract only what is different
 *	- a search for paths alters the Station objects: should not
 *	- the location concept should be the input for the path search. As it is now it looks that 
 *    Station is both a Station and a node in the Graph, substituting the location in searches
 *	- the diagram has too many compositions...some are aggregations, also, the direction of 
 *    the associations is inverse
 *  - inheritance represented as a composition in the diagram
 *	- no answer for searching the available paths
 *	- some members in few classes are public and should not
 * 
 * 
 * Observations updates:
 * 
 *  - i have removed the Route Types because they were not needed anymore,
 *    and i have changed the Route to compute the graph based on the given
 * 	- i have created another object, StationNode in order not to alter the Station
 *    instance any more.
 *  - corrected diagram and updated for the new project
 *  - members corrected
 * 
 * 
 * Project #2 Updates:
 * 
 *  - i have created another application in order to get the distances and the time
 *    between two stations. The application takes the current stations from our database
 *    more exactly is the csv file imported to DB provided, which does querys to 
 *    Google Distance Matrix API, parses the JSON response and populates another table 
 *    in our MySQL database in the same time creating the connections between stations.
 *    This application help us to create a "cache" for the data received.
 *    
 *  - i have changed the Location concept to Area concept which can contain multiple 
 *    stations into an area, which it chooses the route bases on the minimal criteria we have given.
 *
 * 	- 
 * ce mai am de facut
 * - documentatia
 * - diagrama
 * - de explicat geo-distance
 * -
 * 
 * 
 * @author Mihai Adrian Maghiar on Jan 14, 2015
 * 
 */

public class PublicTransportNetwork {

	public static void main(String[] args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		DbManager connect = new DbManager();
		Connection connection = connect.getConnection();
		InputDevice iD = new DbInput(connection);
		OutputDevice oD = new OutputDevice();
		
		AppController appController = new AppController(iD,oD);
		
		try {
			appController.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
		connect.closeConnection(connection);
		
		
	}

}
