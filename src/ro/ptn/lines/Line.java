package ro.ptn.lines;

import java.util.List;

import ro.ptn.lines.connections.Connection;
import ro.ptn.lines.connections.IConnection;
import ro.ptn.stations.IStation;
/**
 * This class "Lines" refers to the all stations and connections that 
 * belongs to a line
 * still needs to be implemented
 * @author Mihai Adrian Maghiar on Nov 21, 2014
 * @since  21.11.2014
 */

public class Line {
	
	private String name;
	private String type;
	private List<IStation> lineStation;
	private List<IConnection> lineConnection;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<IStation> getLineStation() {
		return lineStation;
	}
	public void setLineStation(List<IStation> lineStation) {
		this.lineStation = lineStation;
	}
	public List<IConnection> getLineConnection() {
		return lineConnection;
	}
	public void setLineConnection(List<IConnection> lineConnection) {
		this.lineConnection = lineConnection;
	}
}
