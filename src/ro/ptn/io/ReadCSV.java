package ro.ptn.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadCSV {
	
	public String inputFile;
	List<String> Data = new ArrayList<String>();
	
	public ReadCSV(String CSVFile){
		this.inputFile=CSVFile;
	}
	
	public List<String> readCSVFile() throws Exception{
		
		//read csv configuration
		//String file = "stationList.csv";
		BufferedReader br = null;
		String line ;
		

		try {
			br = new BufferedReader(new FileReader(this.inputFile));
			br.readLine();
			while ((line = br.readLine()) != null) {
				
				this.Data.add(line);
				
			}
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			if (br != null){
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		return Data;
	}

}
