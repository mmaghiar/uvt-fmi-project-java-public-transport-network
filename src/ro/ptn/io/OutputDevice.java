package ro.ptn.io;

import java.util.List;

import ro.ptn.routes.StationNode;

/**
 * This class prints the paths found in the graph
 * @author Mihai Adrian Maghiar on Dec 5, 2014
 * @since  21.11.2014
 */

public class OutputDevice {
	
	public String printType(String costType){
		
		if(costType=="price"){
			return costType="Price";
		} else if(costType=="lenght"){
			return costType="Kilometer(s)";
		} else if(costType=="time"){
			return costType="Minutes";
		}
		
		return costType;
		
	}
	
	public void printRoute(List<StationNode> stations, String costType){
		System.out.println("\n--------------- "+costType+" Route ---------------");
		for(StationNode s : stations){
			System.out.println("StationID: " + s.getStation().getId() + " StationName: "+ s.getStation().getStationName()
					+ " Line: " + s.getLastLine() + " " + printType(costType) + " " + s.getCost(costType));
		}
	}
	
	
}
