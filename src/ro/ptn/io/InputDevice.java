package ro.ptn.io;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ro.ptn.lines.connections.Connection;
import ro.ptn.stations.Station;

/**
 * This class reads the data from the file and populates the graph
 * @author Mihai Adrian Maghiar on Dec 4, 2014
 * @since  21.11.2014
 */

public class InputDevice {
	
	List<String> data;
	
	// read the data from file method
	public List<String> readFile(String fileName) throws Exception{
		ReadCSV readCSV = new ReadCSV(fileName);
		return this.data = readCSV.readCSVFile(); 
	}

	public Map<String, Station> getStations(List<String> data){
		
		String cvsSplitBy = ",";
		Station s = null;
		
		Map<String,Station> stationList = new HashMap<String, Station>();
		
		for(String line : data){
			
			//split the data from csv 
			String[] stationTokens = line.split(cvsSplitBy);
			
			//get the station ID from the list
			s = stationList.get(stationTokens[0]);
			
			//initialiaze the station if is not created 
			if(s == null){
				//creating station 
				s = new Station(stationTokens[0],stationTokens[1]);
				
				//this adds the station into the list
				stationList.put(stationTokens[0], s);
			} 
			//this sets the station name
			s.setStationName(stationTokens[1]);
		
			Station connectionStation = stationList.get(stationTokens[6]);
			
			if(connectionStation == null){
				connectionStation = new Station(stationTokens[6]);
				
				stationList.put(stationTokens[6], connectionStation);
			}
			
			Connection c = new Connection(connectionStation);
			
			c.setLineNumber(stationTokens[2]);
			c.getCost().put("speed", Double.parseDouble(stationTokens[3]));
			c.getCost().put("lenght", Double.parseDouble(stationTokens[4]));
			c.getCost().put("price", Double.parseDouble(stationTokens[5]));
			c.getCost().put("time", 3.3);			
			s.addConnection(c);
		}
		
		return stationList;
	}
	public Map<String, Station> getStations()throws Exception{
		return null;
	};
	
	//this reads the input station
	public String getStation(String node){
		
	//	System.out.println(msg);
	///	String station = in.next();
		String station = node;
		return station;
	}
}