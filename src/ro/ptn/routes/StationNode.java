package ro.ptn.routes;

import ro.ptn.stations.IStation;

/**
 * 
 * @author Mihai Adrian Maghiar on Jan 28, 2015
 * @since  21.11.2014
 */

public class StationNode implements Comparable<StationNode>{
	private IStation station;
	
	public double minDistance = Double.POSITIVE_INFINITY;
	private double minPrice = Double.POSITIVE_INFINITY;
	private double minSpeed = Double.POSITIVE_INFINITY;
	private double minTime = Double.POSITIVE_INFINITY;
	private String lastLine = null;
	
	public StationNode previous;
	
	public StationNode(IStation s){
		this.setStation(s);
		minDistance = Double.POSITIVE_INFINITY;
		minPrice = Double.POSITIVE_INFINITY;
		minSpeed = Double.POSITIVE_INFINITY;
		minTime = Double.POSITIVE_INFINITY;
	}
	public int compareTo(StationNode another) {
		return Double.compare(minDistance, another.minDistance);
	}
	
	public double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}
	public double getMinSpeed() {
		return minSpeed;
	}
	public void setMinSpeed(double minSpeed) {
		this.minSpeed = minSpeed;
	}
	public double getMinTime() {
		return minTime;
	}
	public void setMinTime(double minTime) {
		this.minTime = minTime;
	}
	public IStation getStation() {
		return station;
	}
	public void setStation(IStation s) {
		this.station = s;
	}
	public String getLastLine() {
		return lastLine;
	}
	public void setLastLine(String lastLine) {
		this.lastLine = lastLine;
	}
	
	public double getCost(String costType){
		if(costType.equals("lenght")){
			return minDistance;
		}
		if(costType.equals("price")){
			return minPrice;
		}
		if(costType.equals("time")){
			return minTime;
		}
		return 0;
	}
	public double setCost(String costType, double cost){
		if(costType.equals("lenght")){
			minDistance = cost;
		}
		if(costType.equals("price")){

			minPrice = cost;
		}
		if(costType.equals("time")){
			minTime = cost;
		}
		return 0;
	}
	
	public boolean equals(Object o){
		if(o instanceof StationNode){
			StationNode sn = (StationNode) o;
			if(sn.getStation().getId().equals(this.station.getId()))
				return true;
		}
		return false;
	}
}
