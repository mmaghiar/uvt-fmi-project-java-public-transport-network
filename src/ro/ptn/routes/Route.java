package ro.ptn.routes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import ro.ptn.area.Area;
import ro.ptn.lines.connections.IConnection;
import ro.ptn.stations.IStation;
import ro.ptn.stations.Station;

/**
 * The represents the implementation of Dijkstra algorithm
 * needed to find the path based on the criteria required.
 * 
 * Project #2 Updates:
 * 
 * This time we say what kind of route we want to compute.
 * I tried to remove the copy-paste classes and implemented 
 * this version.
 * 
 * 
 * @author Mihai Adrian Maghiar on Ian 12, 2015
 * @since  21.11.2014
 */

public class Route {

	public static StationGraph computeGraph(IStation s, String costType){
		StationGraph sg = new StationGraph();
		double distanceThroughtU = 0.0;
		StationNode source = new StationNode(s);
		source.setCost(costType, 0);
		PriorityQueue<StationNode> stationQueue = new PriorityQueue<StationNode>();
		stationQueue.add(source);
		
		while(!stationQueue.isEmpty()){
			StationNode u = stationQueue.poll();
			
			//visit each edge exiting u
			for (IConnection e : u.getStation().getConnections()){ // get list of adjacencies
				StationNode v = sg.nodes.get(e.getDestinationStation().getId());
				if(v == null)
					v = new StationNode(e.getDestinationStation());
//				if(sg.nodes.get(v.getStation().getId()) == null && e.getLineNumber().equals(LASTLINE))
//					continue;

				distanceThroughtU = u.getCost(costType) + e.getCost().get(costType);

				if(distanceThroughtU < v.getCost(costType)){
					stationQueue.remove(v);
					v.setCost(costType, distanceThroughtU);
					v.previous = u;
					v.setLastLine(e.getLineNumber());
					if(!stationQueue.contains(v)) // process only 1 time each station to avoid
						stationQueue.add(v);	// multiple station processing
					
				}
			}
			sg.nodes.put(u.getStation().getId(), u);
		}
		return sg;
	}

	// Here we make overload of the method in order to receive another type of parameter

	public static StationGraph computeGraph(Area l, String costType){
		double best = Integer.MAX_VALUE;
		StationGraph bestSG = null;

		// For every station we compute a station and return the graph 
		// that has the smallest maximum cost . 
		
		for(Station s : l.getAreaStations()){
			StationGraph sg = computeGraph(s, costType); 
			if(sg.maxCost(costType) <= best){
				best = sg.maxCost(costType);
				bestSG = sg;
			}
		}
		return bestSG;
	}
		
	//
	
	public static List<StationNode> getPath(IStation s, StationNode destination){
		List<StationNode> path = new ArrayList<StationNode>();
		for(StationNode station = destination; station != null; station = station.previous){
			path.add(station);
		}
		Collections.reverse(path);
		return path;
	}
	
	//
	
	public static List<StationNode> getPath(Area l, StationNode destination){
		List<StationNode> path = new ArrayList<StationNode>();
		for(StationNode station = destination; station != null; station = station.previous){
			path.add(station);			
		}
	
		Collections.reverse(path);
		return path;
	}
}
