package ro.ptn.lines.connections;

import java.util.Map;

import ro.ptn.stations.IStation;

public interface IConnection {
	public String getLineNumber();
	public void setLineNumber(String lineNumber);
	public IStation getDestinationStation();
	public void setDestinationStation(IStation targetStation);
	public boolean equals(Connection c);
	public Map<String, Double> getCost();
	public void setCost(Map<String, Double> cost);
}
