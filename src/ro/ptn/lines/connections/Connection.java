package ro.ptn.lines.connections;

import java.util.HashMap;
import java.util.Map;

import ro.ptn.stations.IStation;
import ro.ptn.stations.Station;

/**
 * This class represents every connection between every station, 
 * also walkways and it is the EDGE of the graph
 * 
 * @author Mihai Adrian Maghiar on Nov 25, 2014
 * @since  21.11.2014
 */

public class Connection implements IConnection{

	private IStation destinationStation; 
	private Map<String, Double> cost = new HashMap<String, Double>(); 
	private String lineNumber;

	public Connection(IStation targetStation){
		this.setDestinationStation(targetStation);
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public IStation getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(IStation targetStation) {
		this.destinationStation = targetStation;
	}
	
	public boolean equals(Connection c){
		if(c == null)
			return false;
		return this.lineNumber.equals(c.getLineNumber());
	}

	public Map<String, Double> getCost() {
		return cost;
	}

	public void setCost(Map<String, Double> cost) {
		this.cost = cost;
	}
}
