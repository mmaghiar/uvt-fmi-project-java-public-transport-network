package ro.ptn.routes;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Mihai Adrian Maghiar on Jan 21, 2015
 * @since  21.11.2014
 */
public class StationGraph {
	
	public Map<String, StationNode> nodes = new HashMap<String,StationNode>();
	
	public double maxCost(String costType){
		double max = 0;
		for(StationNode sn : nodes.values()){
			if(max > sn.getCost(costType)){
				max = sn.getCost(costType);
			}
		}
		return max;
	}
	
}
