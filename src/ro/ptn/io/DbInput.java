package ro.ptn.io;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ro.ptn.lines.connections.Connection;
import ro.ptn.stations.Station;

/**
 * This class takes the necessary data from database to
 * populate the graph
 * @author Mihai Adrian Maghiar on Jan 14, 2015
 * @since  21.11.2014
 */
public class DbInput extends InputDevice{
	
	List<String> data;
	private java.sql.Connection connection;
	// read the data from file method
	public DbInput(java.sql.Connection conn){
		this.setConnection(conn);
	}

	@Override
	public Map<String, Station> getStations() throws SQLException{
		
		
		Statement sqlStatement = connection.createStatement();
		ResultSet stationResultSet = sqlStatement.executeQuery("SELECT * FROM Stations"); 		
		 
		
		Map<String,Station> stationList = new HashMap<String, Station>();
		
		while(stationResultSet.next()){
			String stationName= stationResultSet.getString("RawStationName");
			String stationId = stationResultSet.getString("StationID");
			String StationLineName= stationResultSet.getString("LineName");
			String StationLineId= stationResultSet.getString("LineID");
			boolean valid = !stationResultSet.getString("Invalid").equals("1");
			if(!valid)
				continue;
			Station newStation = stationList.get(stationId);
			
			if(newStation == null){
				//creating station 
				newStation = new Station(stationId,stationName);
				
				//this adds the station into the list
				stationList.put(stationId, newStation);
			} 
			
			//this sets the station name
			newStation.setStationName(stationName);
			
		}
		
		ResultSet connectionResultSet = sqlStatement.executeQuery("SELECT * FROM Connection");
		
		while(connectionResultSet.next()){
			Station start = stationList.get(connectionResultSet.getString("StartStationID"));			
			Station end = stationList.get(connectionResultSet.getString("EndStationID"));
			if(start == null || end == null)
				continue;
			String lineID = connectionResultSet.getString("LineID");
			String distance = connectionResultSet.getString("Distance");
			String duration = connectionResultSet.getString("Time");
			
			Connection c = new Connection(end);
			
			c.setLineNumber(lineID);
			c.getCost().put("speed", Double.parseDouble(distance) / Double.parseDouble(duration));
			c.getCost().put("lenght", Double.parseDouble(distance));
			c.getCost().put("price", Double.parseDouble("2.5")) ;
			c.getCost().put("time", Double.parseDouble(duration));			
			start.addConnection(c);
			
		}
		
		return stationList;
	}
	
	//this reads the input station
	

	public java.sql.Connection getConnection() {
		return connection;
	}

	public void setConnection(java.sql.Connection connection) {
		this.connection = connection;
	}
}
