package ro.ptn.stations;

import java.util.List;

import ro.ptn.lines.connections.Connection;

public interface IStation {
	public String getId();
	public void setId(String id);
	public String getStationName();
	public void setStationName(String stationName);
	public void addConnection(Connection c);
	public List<Connection> getConnections();
}
