package ro.ptn.app.controller;

import java.util.List;
import java.util.Map;

import ro.ptn.area.Area;
import ro.ptn.io.InputDevice;
import ro.ptn.io.OutputDevice;
import ro.ptn.routes.Route;
import ro.ptn.routes.StationGraph;
import ro.ptn.routes.StationNode;
import ro.ptn.stations.Station;

/**
 * This class manages the input and output of the program
 * and also provides the main functionality
 * 
 * @author Mihai Adrian Maghiar on Jan 14, 2015
 * @since  21.11.2014
 */

public class AppController {
	
	private InputDevice inputDevice;
	private OutputDevice outputDevice;

	public AppController(InputDevice id, OutputDevice od){
		this.inputDevice = id;
		this.outputDevice = od;
	}
	
	public void run() throws Exception{
		
		Map<String, Station> stationsMap = null;
		
		// we are not interested from which input we read, we care just that
		// we read from an input
		stationsMap = inputDevice.getStations();
		
		System.out.println("Finished reading the database.");
		
		//Starting from a single station
		String START_ID = "2694"; // starting station id
		String END_ID = "2687"; //end station id
		
		StationGraph sr =  Route.computeGraph(stationsMap.get(START_ID),"lenght");
		List<StationNode> computedShortestRoute = Route.getPath(stationsMap.get(START_ID), sr.nodes.get(END_ID));
		//System.out.println(sr.nodes.size());
		outputDevice.printRoute(computedShortestRoute,"lenght");
		
		StationGraph cr =  Route.computeGraph(stationsMap.get(START_ID),"price");
		List<StationNode> computedCheapestRoute = Route.getPath(stationsMap.get(START_ID), cr.nodes.get(END_ID));
		outputDevice.printRoute(computedCheapestRoute,"price");
		
		StationGraph qr =  Route.computeGraph(stationsMap.get(START_ID),"time");
		List<StationNode> computedQuickestRoute = Route.getPath(stationsMap.get(START_ID), qr.nodes.get(END_ID));
		outputDevice.printRoute(computedQuickestRoute,"time");
		
		//Add as many stations into an Area
		//The getPath method will return the shortest route(graph).
		Area location = new Area();
		location.setAreaName("Sagului");
		location.addStation(stationsMap.get("2799"));
		location.addStation(stationsMap.get("2666"));
		location.addStation(stationsMap.get("2667"));
		
		StationGraph sg2 =  Route.computeGraph(location,"time");
		List<StationNode> computedRouteArea = Route.getPath(location, sg2.nodes.get(END_ID));
		
		outputDevice.printRoute(computedRouteArea,"time");
		

	}
	
}
