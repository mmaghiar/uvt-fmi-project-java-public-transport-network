package ro.ptn.area;
import java.util.ArrayList;
import java.util.List;

import ro.ptn.stations.Station;
/**
 * This class models the stations located in a area
 * 
 * Update:
 * If there is a location where is not selected a station, 
 * then the route will be computed the shortest route possible from the stations
 * 
 * @author Mihai Adrian Maghiar on Jan 17, 2015
 * @since  21.11.2014
 */

public class Area {

	private String areaName;
	private List<Station> areaStations = new ArrayList<Station>();
	
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public List<Station> getAreaStations() {
		return areaStations;
	}
	public void setAreaStations(List<Station> areaStations) {
		this.areaStations = areaStations;
	}
	public void addStation(Station s){
		areaStations.add(s);
	}
	
}
