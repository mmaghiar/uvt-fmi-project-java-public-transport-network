package ro.ptn.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *  This class is used to load the MySQL driver and create the connection
 *  to the database.
 *  The login credentials are still valid.
 *  
 * @author Mihai Adrian Maghiar on Jan 14, 2015
 * @since  21.11.2014
 */
public class DbManager {

	public void closeConnection(Connection c){
		try{
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = null;
	 
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://projectpj.info.uvt.ro:3306/Adi_Java_PTR","ptr", "G&BDs$K2a4D5");
	 
		} catch (SQLException e1) {
			System.out.println("Connection Failed!");
			e1.printStackTrace();
		}
	 
		if (connection != null) {
			System.out.println("Connected to database.");
		} else {
			System.out.println("Failed to connect!");
		}
	
		return connection;
	}
}