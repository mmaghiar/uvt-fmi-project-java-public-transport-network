package ro.ptn.stations;

import java.util.ArrayList;
import java.util.List;

import ro.ptn.lines.connections.Connection;

/**
 * This class represents the model of a station
 * 
 * @author Mihai Adrian Maghiar on Jan 22, 2015
 * @since  21.11.2014
 */
public class Station implements IStation{
	
	private String id; // station uniq id
	private String stationName; //station uniq name
	private List<Connection> adjacencies = new ArrayList<Connection>();
	//TODO Get rid of these as they will be kept inside the StationNode.	
	
	
	public Station(String id, String stationName){
		this.setId(id);
		this.setStationName(stationName);
	}
	public Station(String id){
		this.setId(id);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	//adding connection for every neighbour station
	public void addConnection(Connection c){
		adjacencies.add(c);
	}
	
	public List<Connection> getConnections(){
		
		return adjacencies;
	}
}